# Encrypted MySQL Dump

This modules provides the Drush command sql:encryptdump/sql-encryptdump.

This command is complete compatible with sql-dump, but produces an encrypted file.

## Requirements / Restrictions

Currently, this only works with a MySQL based database.

Openssl must be installed and in the path.


## Usage

The functionally, the command works exactly like `drush sql:dump`, but using `drush sql:encryptdump`.

However, you're required to give `drush sql:encryptdump` a passcode to derive an encryption key and salt from.

There are two ways of passing the passcode to `drush sql:encryptdump`:

1. Via the `--encrypt` option.  I.E. `drush sql:encryptdump --encrypt=passcode`
2. Via the environment variable `DRUSH_SQL_DUMP_ENCRYPT_KEY`.

If you're using the environment variable, ensure that PHP if configured to read environment variables. See: https://www.php.net/manual/en/ini.core.php#ini.variables-order

## Encryption

The SQL dump file is encrypted with a salted AES-256 CBC, using PBKDF2 for key and salt derivation.

The encryption of the file is equivalent to running this command: `openssl aes-256-cbc -e -a -salt -pbkdf2`

## Decryption

The resulting SQL dump file can be decrypted with:

```openssl aes-256-cbc -d -a -salt -pbkdf2 -in database_dump_file -out decrypted_database_dump_file```

You absolutely should test that you can decrypt your database files before using this module regularly.
