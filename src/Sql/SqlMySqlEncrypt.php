<?php

namespace Drupal\encrypted_sqldump\Sql;

use Drush\Sql\SqlMysql;
use Drush\Sql\SqlBase;
use Drush\Drush;
use Consolidation\SiteProcess\Util\Escape;
use Drupal\Core\Database\Database;

class SqlMySqlEncrypt extends SqlMysql {

    /**
     * Get a driver specific instance of this class.
     *
     * @param $options
     *   An options array as handed to a command callback.
     */
    public static function create(array $options = []): ?SqlBase
    {
        // Set defaults in the unfortunate event that caller doesn't provide values.
        $options += [
            'database' => 'default',
            'target' => 'default',
            'db-url' => null,
            'databases' => null,
            'db-prefix' => null,
        ];
        $database = $options['database'];
        $target = $options['target'];

        if ($url = $options['db-url']) {
            $url = is_array($url) ? $url[$database] : $url;
            $db_spec = self::dbSpecFromDbUrl($url);
            $db_spec['prefix'] = $options['db-prefix'];
            return self::getInstance($db_spec, $options);
        } elseif (($databases = $options['databases']) && (array_key_exists($database, $databases)) && (array_key_exists($target, $databases[$database]))) {
            // @todo 'databases' option is not declared anywhere?
            $db_spec = $databases[$database][$target];
            return self::getInstance($db_spec, $options);
        } elseif ($info = Database::getConnectionInfo($database)) {
            $db_spec = $info[$target];
            return self::getInstance($db_spec, $options);
        } else {
            throw new \Exception(dt('Unable to load Drupal settings. Check your --root, --uri, etc.'));
        }
    }


    public static function getInstance($db_spec, $options): ?self
    {
        $instance = new self($db_spec, $options);
        $instance->setConfig(Drush::config());
        return $instance;
    }

    /*
     * Execute a SQL dump and return the path to the resulting dump file.
     *
     * @return
     *   Returns path to dump file, or false on failure.
     */
    public function dump()
    {
        /** @var string|bool $file Path where dump file should be stored. If TRUE, generate a path based on usual backup directory and current date.*/
        $file = $this->getOption('result-file');
        $key = $this->loadKey();

        $file_suffix = '';
        $table_selection = $this->getExpandedTableSelection($this->getOptions(), $this->listTables());
        $file = $this->dumpFile($file);
        $cmd = $this->dumpCmd($table_selection);
        $pipefail = '';
        // Gzip the output from dump command(s) if requested.
        if ($this->getOption('gzip')) {
            // See https://github.com/drush-ops/drush/issues/3816.
            $pipefail = $this->getConfig()->get('sh.pipefail', 'bash -c "set -o pipefail; {{cmd}}"');
            $cmd .= " | gzip -f";
            $file_suffix .= '.gz';
        }
        $command = 'openssl';
        if (NULL !== $this->getOption('openssl')) {
          $command = escapeshellcmd($this->getOption('openssl'));
        }

        $cmd .= " | $command aes-256-cbc -e -a -salt -pbkdf2 -k '$key'";

        if ($file) {
            $file .= $file_suffix;
            $cmd .= ' > ' . Escape::shellArg($file);
        }
        $cmd = $this->addPipeFail($cmd, $pipefail);

        $process = Drush::shell($cmd, null, $this->getEnv());
        // Avoid the php memory of saving stdout.
        $process->disableOutput();
        // Show dump in real-time on stdout, for backward compat.
        $process->run($process->showRealtime());
        return $process->isSuccessful() ? $file : false;
    }

    public function loadKey() {
      $key = isset($_ENV['DRUSH_SQL_DUMP_ENCRYPT_KEY']) ? $_ENV['DRUSH_SQL_DUMP_ENCRYPT_KEY'] : $this->getOption('encrypt');
      if (empty($key)) {
        throw new \Exception(dt("Encryption key not found. The encryption key must be defined in either an Environment variable named DRUSH_SQL_DUMP_ENCRYPT_KEY or passed with the --encrypt=key option.\nIf you're using the environment variable, ensure that PHP if configured to read environment variables. See: https://www.php.net/manual/en/ini.core.php#ini.variables-order "));
      }
    }
}
