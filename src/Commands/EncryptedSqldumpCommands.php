<?php

namespace Drupal\encrypted_sqldump\Commands;


use Consolidation\OutputFormatters\StructuredData\PropertyList;
use Drupal\encrypted_sqldump\Sql\SqlMySqlEncrypt;
use Drush\Commands\DrushCommands;
use Drush\Drush;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class EncryptedSqldumpCommands extends DrushCommands {

    /**
     * Exports the Drupal DB as SQL using mysqldump or equivalent.
     *
     * @command sql:encryptdump
     * @aliases sql-encryptdump
     * @optionset_sql
     * @optionset_table_selection
     * @option result-file Save to a file. The file should be relative to Drupal root. If --result-file is provided with the value 'auto', a date-based filename will be created under ~/drush-backups directory.
     * @option create-db Omit DROP TABLE statements. Used by Postgres and Oracle only.
     * @option data-only Dump data without statements to create any of the schema.
     * @option ordered-dump Order by primary key and add line breaks for efficient diffs. Slows down the dump. Mysql only.
     * @option gzip Compress the dump using the gzip program which must be in your <info>$PATH</info>.
     * @option extra Add custom arguments/options when connecting to database (used internally to list tables).
     * @option extra-dump Add custom arguments/options to the dumping of the database (e.g. <info>mysqldump</info> command).
     * @option encrypt Encrypt using the provided key --encrypt=passcode
     * @option openssl OpenSSL path to override the default openssl comand
     * @usage drush sql:dump --result-file=../18.sql
     *   Save SQL dump to the directory above Drupal root.
     * @usage drush sql:dump --skip-tables-key=common
     *   Skip standard tables. See [Drush configuration](../using-drush-configuration)
     * @usage drush sql:dump --extra-dump=--no-data
     *   Pass extra option to <info>mysqldump</info> command.
     * @hidden-options create-db
     * @bootstrap max configuration
     * @field-labels
     *   path: Path
     *
     *
     * @notes
     *   --createdb is used by sql-sync, since including the DROP TABLE statements interferes with the import when the database is created.
     */
    public function dump($options = ['encrypt' => NULL, 'result-file' => self::REQ, 'create-db' => false, 'data-only' => false, 'ordered-dump' => false, 'gzip' => false, 'extra' => self::REQ, 'extra-dump' => self::REQ, 'format' => 'null', 'openssl' => NULL]): PropertyList
    {
        if (!isset($options['openssl'])) {
          $p = Drush::shell('which openssl');
        } else {
          $command = escapeshellcmd($options['openssl']);
          $p = Drush::shell('which ' . $command);
        }
        $p->run();
        if (!$p->isSuccessful()) {
            throw new \Exception('The command openssl was not found in the path, please install openssl.');
        }
        $sql = SqlMySqlEncrypt::create($options);
        $return = $sql->dump();
        if ($return === false) {
            throw new \Exception('Unable to dump database. Rerun with --debug to see any error message.');
        }

        // SqlBase::dump() returns null if 'result-file' option is empty.
        if ($return) {
            $this->logger()->success(dt('Database dump saved to !path', ['!path' => $return]));
        }
        return new PropertyList(['path' => $return]);
    }

}
